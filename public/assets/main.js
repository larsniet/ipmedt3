window.onload = () => {
    // // // //  //
    // Variables //
    // // // //  //

    console.log = function() {}

    const camera = document.querySelector('#rig');
    const camera2 = document.querySelector("#js--camera");

    const startScene = document.querySelector('#js--startScene');
    const startKnop = document.querySelector('#js--startKnop');
    const restartKnop = document.querySelector('#js--restartKnop');
    const restartScene = document.querySelector('#js--restartScene');

    const wasMand = document.querySelector('#wasmandModel');
    const wasMandPijl = document.querySelector('#wasmandPijl');

    const glasWater = document.querySelector('#glasWater');
    const waterInGlas = document.querySelector('#waterInGlas');
    const glasPijl = document.querySelector('#glasPijl');

    // const shirtLabel = document.getElementById("#shirtLabel");
    const attentionRing = document.querySelector('#attentionRing');
    const ringHitbox = document.querySelector('#hitbox');

    const kleding = document.querySelector('#kleding');
    const kledingsstukkken = document.querySelector('#kledingsstukken');
    const kledingsstuk = document.querySelectorAll('.kledingsstuk');
    const kledingsstukkenShirt = document.querySelector('#kledingsstukken-shirt');
    const kledingsstukkenShirtLang = document.querySelector('#kledingsstukken-shirtlang');
    const kledingsstukkenBroek = document.querySelector('#kledingsstukken-broek');

    const shirtModelKort = document.getElementById("shirtModelKort");
    const shirtModelLang = document.getElementById("shirtModelLang");
    const shirtMouwenKort = document.getElementById("shirtMouwenKort");
    const shirtMouwenLang = document.getElementById("shirtMouwenLang");
    const shirtRechterMouwKort = document.getElementById("shirtMouwRKort");
    const shirtRechterMouwLang = document.getElementById("shirtMouwRLang");
    const shirtLinkerMouwKort = document.getElementById("shirtMouwLKort");
    const shirtLinkerMouwLang = document.getElementById("shirtMouwLLang");
    const shirtOnderkantKort = document.getElementById("shirtOnderkantKort");
    const shirtOnderkantLang = document.getElementById("shirtOnderkantLang");
    const shirtBovenkantKort = document.getElementById("shirtBovenkantKort");
    const shirtBovenkantLang = document.getElementById("shirtBovenkantLang");
    const shirtMouwOnderLang = document.getElementById("shirtMouwOnder");
    const shirtMouwROnderLang = document.getElementById("shirtMouwROnderLang");
    const shirtMouwLOnderLang = document.getElementById("shirtMouwLOnderLang");
    const langShirt = document.getElementById("langShirt");
    const kortShirt = document.getElementById("kortShirt");

    const supremeLogo = document.querySelector("#supremeLogo");

    const broek = document.querySelector("#Broek");
    const broekspijpOnderLinks = document.querySelector("#BroekspijpOnderLinks");
    const broekspijpOnderRechts = document.querySelector("#BroekspijpOnderRechts");
    const broekspijpBoven = document.querySelector("#BroekspijpBoven");

    const broekspijpenLinks = document.querySelector("#BroekspijpenLinks");
    const broekspijpenRechts = document.querySelector("#BroekspijpenRechts");
    const broekspijpenRechts2 = document.querySelector("#BroekspijpenRechts2");

    const stopContact = document.querySelector('#stopContact');
    const stekkerPijl = document.querySelector('#stekkerPijl');
    const stekkerGrond = document.querySelector('#stekkerGrond');
    const stekkerMuur = document.querySelector('#stekkerMuur');
    const kabel = document.querySelector('#kabelGrond');
    const kabel2 = document.querySelector('#kabelMuur');

    const strijkplank = document.querySelector('#plank');
    const helePlank = document.querySelector('#helePlank');
    const plankPijl = document.querySelector('#strijkplankPijl');

    const klink = document.querySelector('#klink');

    const ijzer = document.querySelector('#strijkIjzer');
    const ijzerPijl = document.querySelector('#strijkijzerPijl');
    const ijzerKnop = document.querySelector('#strijkIjzerKnop');
    const ijzerKnopStatic = document.querySelector('#strijkIjzerKnopStatic');
    const strijkIjzerZonderKnop = document.querySelector('#strijkIjzerZonderKnop');
    const strijkIjzerParticle = document.querySelector('#strijkIjzerParticle');

    const shirtStrijkLabel = document.querySelector('#shirtStrijkLabel');

    const instellingen = document.querySelector('#instellingen');
    const opties = document.querySelectorAll('.optie');
    const bevestigen = document.querySelector('#bevestigen');

    const scene1 = document.querySelector('#scene1');
    const scene2 = document.querySelector('#scene2');
    const lamp = document.querySelector('#lampPosition');

    const hintText = document.querySelector('#hintText');
    const hintBackground = document.querySelector('#hintBackground');

    const kastDeurR = document.querySelector('#kastDeurR');
    const kastDeurL = document.querySelector('#kastDeurL');
    const kastPijl = document.querySelector('#kastPijl');
    const rechterStandaard = document.querySelector('#rechterStandaard');
    const linkerStandaard = document.querySelector('#linkerStandaard');

    const stoomKnop = document.querySelector('#stoomKnop');
    const stoomKnopAchtergrond = document.querySelector('#stoomKnopAchtergrond');

    const taal = 'Dutch Female';
    const rate = {
        rate: 0.9
    };

    const shirtKort = {
        shirtModel: shirtModelKort,
        shirtMouwen: shirtMouwenKort,
        shirtRechterMouw: shirtRechterMouwKort,
        shirtLinkerMouw: shirtLinkerMouwKort,
        shirtOnderkant: shirtOnderkantKort,
        shirtBovenkant: shirtBovenkantKort,
        shirtMouwOnder: null,
        shirtMouwROnder: null,
        shirtMouwLOnder: null
    };
    const shirtLang = {
        shirtModel: shirtModelLang,
        shirtMouwen: shirtMouwenLang,
        shirtRechterMouw: shirtRechterMouwLang,
        shirtLinkerMouw: shirtLinkerMouwLang,
        shirtOnderkant: shirtOnderkantLang,
        shirtBovenkant: shirtBovenkantLang,
        shirtMouwOnder: shirtMouwOnderLang,
        shirtMouwROnder: shirtMouwROnderLang,
        shirtMouwLOnder: shirtMouwLOnderLang
    };

    // // // // // // //
    // EventListeners //
    // // // // // // //

    // // Debug Lamp: verander scene snel
    lamp.addEventListener("click", event => {
        changeRoom();
    });

    // Begin spel, tp naar voren toe
    startKnop.addEventListener('click', event => {
        lamp.setAttribute('class', 'clickable'); // DEBUG LAMP?
        deleteStartScene();
        teleportPlayer(0, 3);
        responsiveVoice.speak('Laten we beginnen met de strijkplank uit de kast te halen.', taal, rate);
        kastPijl.setAttribute('visible', 'true');
        setTimeout(function () {
            kastDeurR.setAttribute('class', 'clickable');
        }, 3000);
    });

    // Zet de strijkplank alvast in positie (in de kast)
    helePlank.setAttribute("position", "-9.5 3.2 0.5");
    helePlank.setAttribute("rotation", "180 180 90");
    kastDeurR.addEventListener("click", event => {
        // Uitleg
        responsiveVoice.speak(
            "Zorg er voor dat de strijkplank stevig staat, dit kunt u checken door met matige druk op de bovenkant te duwen.",
            taal,
            rate
        );
        // Verwijder pijl en clickable class
        kastDeurR.setAttribute("class", "none");
        kastPijl.setAttribute("visible", "false");
        // Animeer openen van deur
        animateProperty(0, 20, 0, kastDeurR, 'rotation');
        animateProperty(0, -20, 0, kastDeurL, 'rotation');

        // Animeer binnenkomen van strijkplank

        setTimeout(function () {
            animateProperty(0, 3, 0, helePlank, 'position');
        }, 2200);

        setTimeout(function () {
            animateProperty(0, -90, 0, kastDeurR, 'rotation');
            animateProperty(180, 180, 180, helePlank, 'rotation');
        }, 4000);
        setTimeout(function () {
            animateProperty(0, -90, 0, rechterStandaard, 'rotation');
        }, 6000);
        setTimeout(function () {
            animateProperty(0, -90, 0, linkerStandaard, 'rotation');
        }, 6000);
        setTimeout(function () {
            animateProperty(-3, 0, 0, rechterStandaard, 'position');
        }, 6000);
        setTimeout(function () {
            animateProperty(-3, 0, 0, linkerStandaard, 'position');
        }, 7000);
        setTimeout(function () {
            animateProperty(0, 0, 0, helePlank, 'position');
        }, 8500);
        // Zet speler nog iets verder naar voren zodat de stekker zichtbaar wordt
        setTimeout(function () {
            teleportPlayer(0, 1.65);
        }, 8800);
        // Laat strijk-attributen in beeld komen
        setTimeout(function () {
            ijzer.setAttribute('visible', 'true');
            kabel.setAttribute('visible', 'true');
            stekkerGrond.setAttribute('visible', 'true');
            responsiveVoice.speak(
                'Om de kleding te kunnen stomen is het van belang dat het waterreservoir gevuld word.',
                taal,
                rate
            );
            glasPijl.setAttribute('visible', 'true');
            // glasWater.setAttribute('class', 'clickable');
        }, 11000);

        setTimeout(function () {
            glasWater.setAttribute('class', 'clickable');
        }, 16000);
    });

    glasWater.addEventListener('click', event => {
        // Vertel over de stoomfunctie
        responsiveVoice.speak(
            'Door uw kleren te stomen gaat het strijken een stuk sneller en gemakkelijker. Zorg er dus voor dat het waterreservoir altijd goed gevuld word.',
            taal,
            rate
        );
        // Maak het glas niet meer clickable en doe de pijl weg
        glasWater.setAttribute('class', '');
        glasPijl.setAttribute('visible', 'false');
        // Doe de animatie
        animateProperty(3.3, 2.6, 0, glasWater, 'position');
        animateProperty(3.3, 3.1, 0, waterInGlas, 'position');

        setTimeout(function () {
            animateProperty(0, 270, 0, glasWater, 'rotation');
            animateProperty(0, 270, 0, waterInGlas, 'rotation');
        }, 2050);
        setTimeout(function () {
            animateProperty(50, 270, 0, glasWater, 'rotation');
            animateProperty(3.1, 2.7, 0, waterInGlas, 'position');
            waterInGlas.setAttribute('scale', '.27 .27 .27');
        }, 4050);
        setTimeout(function () {
            animateProperty(0, 270, 0, glasWater, 'rotation');
            waterInGlas.remove();
        }, 8000);
        setTimeout(function () {
            animateProperty(5, 5.1, -7.7, glasWater, 'position');
            responsiveVoice.speak('Uiteraard is er ook stroom nodig voor het strijkijzer.', taal, rate);
            // stekkerGrond.setAttribute('class', 'clickable');
            stekkerPijl.setAttribute('visible', 'true');
        }, 10000);
        setTimeout(function () {
            animateProperty(5, 5.1, -7.7, glasWater, 'position');
            stekkerGrond.setAttribute('class', 'clickable');
        }, 13000);
    });

    stekkerGrond.addEventListener('click', event => {
        // Stop stekker in het stopcontact en maak wasmand clickable
        stekkerGrond.setAttribute('class', '');
        stekkerPijl.setAttribute('visible', 'false');
        stekkerPijl.setAttribute('position', '3.8 3 -7.5');
        getStekker();
        wasMandPijl.setAttribute('visible', 'true');
        // Uitleg
        responsiveVoice.speak(
            'Nu dat het ijzer op aan het warmen is, kunnen we alvast wat kleren er bij pakken.',
            taal,
            rate
        );

        setTimeout(function () {
            wasMand.setAttribute('class', 'clickable');
        }, 4000);
    });

    // Geef de gebruiker de optie om een kledingsstuk te kiezen en leg deze op de strijkplank neer
    const generatedLabel = [
        Math.floor(Math.random() * 3) + 1,
        Math.floor(Math.random() * 3) + 1
    ];
    let gekozenKledingLabel = 0;

    wasMand.addEventListener('click', event => {
        responsiveVoice.speak(
            'U kunt nu kiezen welk kledingsstuk u wilt gaan strijken. Deze keuzes krijgt u: normaal t-shirt, een t-shirt met opdruk of een broek.',
            taal,
            rate
        );
        kledingsstukkken.setAttribute('visible', 'true');
        wasMandPijl.remove();
        wasMand.setAttribute('class', 'not-clickable');


        // Maak de kledingsstukken clickable
        for (let i = 0; i < kledingsstuk.length; i++) {
            setTimeout(function () {
                kledingsstuk[i].setAttribute('class', 'clickable');
            }, 8000);
        }
        kledingsstukkenShirt.setAttribute('src', `#labelShirt${generatedLabel[0]}`);
        kledingsstukkenShirtLang.setAttribute('src', `#labelShirtLang${generatedLabel[1]}`);
        kledingsstukkenBroek.setAttribute('src', `#labelBroek1`);
    });

    // Zodra kledingsstuk is gekozen, leg deze neer op de strijkplank en verwijder de anderen
    var kledingsstukkenIndex = [
        kledingsstukkenShirt,
        kledingsstukkenShirtLang,
        kledingsstukkenBroek
    ];
    var gekozenKledingsstuk;
    for (let i = 0; i < kledingsstuk.length; i++) {
        kledingsstuk[i].onclick = event => {
            gekozenKledingsstuk = kledingsstukkenIndex[i];
            if (gekozenKledingsstuk == kledingsstukkenShirt) {
                responsiveVoice.speak(
                    'Je hebt gekozen voor een normaal t-shirt.',
                    taal,
                    rate
                ); 
                kledingsstukkken.remove();
                shirtKort.shirtModel.setAttribute("visible", "true");
                shirtKort.shirtMouwen.setAttribute("visible", "true");
                shirtStrijkLabel.setAttribute("visible", "true");
                shirtStrijkLabel.setAttribute(`src`, `#label${generatedLabel[0]}`);
                gekozenKledingLabel = generatedLabel[0];
                strijkijzerReady();
            } else if (gekozenKledingsstuk == kledingsstukkenShirtLang) {
                responsiveVoice.speak(
                    'Je hebt gekozen voor een t-shirt met lange mouwen en een opdruk.',
                    taal,
                    rate
                ); 
                kledingsstukken.remove();
                shirtLang.shirtModel.setAttribute("visible", "true");
                shirtLang.shirtMouwen.setAttribute("visible", "true");
                shirtLang.shirtMouwOnder.setAttribute("visible", "true");
                shirtStrijkLabel.setAttribute("visible", "true");
                shirtStrijkLabel.setAttribute(`src`, `#label${generatedLabel[1]}`);
                gekozenKledingLabel = generatedLabel[1];
                strijkijzerReady();
            } else {
                responsiveVoice.speak(
                    'Je hebt gekozen voor een broek.',
                    taal,
                    rate
                ); 
                kledingsstukken.remove();
                // broekspijpenLinks.setAttribute("visible", "true");
                // broekspijpenRechts.setAttribute("visible", "true");
                console.log(broek)
                broek.setAttribute('visible','true');

                shirtStrijkLabel.setAttribute('visible', 'true');
                shirtStrijkLabel.setAttribute(`src`, `#label1`);
                gekozenKledingLabel = 1;
                strijkijzerReady();
            }
            // Hier kunnen we iets met het gekozen kledingsstuk doen en andere dingen die nog moeten gebeuren
            ijzerPijl.setAttribute("visible", "true");
            ijzer.setAttribute("class", "clickable");
            strijkplank.setAttribute("class", "not-hoverable");
            strijkIjzerZonderKnop.setAttribute("class", "clickable");
            ijzerKnop.setAttribute("class", "clickable");
        };
    }

    ijzer.addEventListener("click", event => {
        responsiveVoice.speak(
            'In het gekozen kledingsstuk zit een label. Op dit label staan stippen die corresponderen met de instellingen van het strijkijzer. Hoe meer stippen hoe hoger de temperatuur van het ijzer.',
            taal,
            rate
        );
        ijzer.setAttribute('class', '');
        strijkIjzerZonderKnop.setAttribute("class", "");
        strijkplank.setAttribute("class", "not-clickable");
        ijzerKnop.setAttribute('class', '');
        ijzerKnop.setAttribute("visible", "false");
        instellingen.setAttribute("visible", "true");
        ijzerPijl.setAttribute("visible", "false");
        setTimeout(function () {
        for (let i = 0; i < opties.length; i++) {
            opties[i].setAttribute("class", "clickable");
        }
        bevestigen.setAttribute("class", "clickable");
        }, 3000);
        // 13000);
    });
    // Mogelijke standen van strijkijzer
    var stands = [-25, -200, -240, -295, -330, -360];
    var gekozenStand;
    for (let i = 0; i < opties.length; i++) {
        opties[i].onclick = event => {
            ijzerKnopStatic.setAttribute(
                `animation`,
                `property: rotation; to: 0 ${stands[i]} 0; dur: 2000; easing: linear`
            );
            gekozenStand = stands[i];
        };
    }

    var correctStand;
    var foutIndex = 0;
    bevestigen.addEventListener("click", event => {
        switch (gekozenKledingLabel) {
            case 1:
                correctStand = -240;
                break;
            case 2:
                correctStand = -295;
                break;
            case 3:
                correctStand = -330;
                break;
        }

        if (gekozenStand == correctStand) {
            responsiveVoice.speak(
                'Nu dat het strijkijzer in de juiste stand staat is het tijd om te beginnen met strijken. Kijk naar 1 van de rode rondjes op het kledingsstuk om te beginnen.',
                taal,
                rate
            );            
            instellingen.remove();
            ijzerKnop.setAttribute("visible", "true");
            strijken(gekozenKledingsstuk);
        } 
        else {
            foutIndex += 1;
            hintText.setAttribute("visible", "true");
            hintBackground.setAttribute("visible", "true");
            switch (foutIndex) {
                case 1:
                    break;
                case 2:
                    hintText.setAttribute(
                        "value",
                        "Op het label staan stippen, op de knop ook"
                    );
                    responsiveVoice.speak(
                        'Op het label staan stippen, op de knop ook',
                        taal,
                        rate
                    );
                    break;
                case 3:
                    hintText.setAttribute(
                        "value",
                        "Kijk goed naar het aantal stippen op het label"
                    );
                    responsiveVoice.speak(
                        'Kijk goed naar het aantal stippen op het label',
                        taal,
                        rate
                    );
                    break;
                case 4:
                    hintText.setAttribute(
                        "value",
                        "1 stip is Zijde, 2 stippen is Wol en 3 stippen is Katoen"
                    );
                    responsiveVoice.speak(
                        '1 stip is Zijde, 2 stippen is Wol en 3 stippen is Katoen',
                        taal,
                        rate
                    );
                default:
                    break;
            }
        }
    });

    // // // // //
    // FUNCTIES //
    // // // // //

    // Particle functie die particles voor stoom aan zet
    const particleFunction = () => {
        strijkIjzerParticle.setAttribute("visible", "true");
        setTimeout(function () {
            strijkIjzerParticle.setAttribute("visible", "false");
        }, 4000);
    };

    // Verwijder startscene
    const deleteStartScene = () => {
        startScene.remove();
    };

    // Plaats stekker in de hand van speler en verwijder deze van de grond
    const getStekker = () => {
        stekkerGrond.setAttribute("visible", "false");
        kabel.setAttribute("visible", "false");
        kabel2.setAttribute("visible", "true");
        stekkerMuur.setAttribute("visible", "true");
    };

    // Verander van kamer
    const changeRoom = () => {
        setTimeout(function () {
            klink.setAttribute(
                "animation",
                "property: rotation; from: 0 -180 0; to: -45 -180 0; easing: linear; loop: 1; dir: alternate;  dur: 2000; easing: easeInOutSine; elasticity: 700;"
            );
        }, 2500);
        setTimeout(function () {
            camera2.innerHTML +=
                '<a-entity position="0 0 -0.6" id="fading-cube" geometry="primitive: box; width: 0.6; height: 0.3;" material="opacity: 0"; animation="property: material.opacity; type: opacity; to: 1; dur: 4000; easing: linear; dir: alternate; loop: 1;"></a-entity>';
        }, 4500);
        setTimeout(function () {
            scene1.setAttribute("visible", "false");
            scene2.setAttribute("visible", "true");
        }, 8490);
        setTimeout(function () {
            responsiveVoice.speak(
                'Bedankt voor het spelen! Succes met strijken.',
                taal,
                rate
            );  
        }, 10000);
        setTimeout(function () {
            location.reload();
        }, 20000)
        teleportPlayer(0, 4);
        strijkplank.setAttribute("class", "not-clickable");
        shirtModelKort.setAttribute("class", "not-clickable");
        shirtMouwenKort.setAttribute("class", "not-clickable");
        ijzer.setAttribute("class", "not-clickable");
    };

    // Code to tp/rotate objects/players to certain points/rotations
    const speed = 2000;
    const teleportPlayer = (x, z) => {
        let att = document.createAttribute('animation');
        att.value = 'property: position; easing: linear; dur: ' + speed + '; to: ' + x + ' 2.4 ' + z;
        camera.setAttribute('animation', att.value);
    };
    let animationCounter = 0;
    const animateProperty = (x, y, z, id, property) => {
        let att;
        let animation;
        if (!id.hasAttribute('animation')) {
            animation = 'animation';
            att = document.createAttribute(animation);
        } else {
            animation = `animation__${animationCounter}`;
            att = document.createAttribute(animation);
        }
        att.value = 'property: ' + property + '; easing: linear; dur: ' + speed + '; to: ' + x + ' ' + y + ' ' + z;
        id.setAttribute(animation, att.value);
        animationCounter++;
    };

    //Strijkijzer klaarmaken
    const strijkijzerReady = () => {
        ijzerPijl.setAttribute("visible", "true");
        ijzer.setAttribute("class", "clickable");
        ijzerKnop.setAttribute("class", "clickable");
        strijkIjzerZonderKnop.setAttribute("class", "clickable");
    };

    //opvouwen mouwen
    const mouwenOpvouwen = (shirt) => {
        shirt.shirtLinkerMouw.classList.replace("not-clickable", "clickable");
        shirt.shirtLinkerMouw.setAttribute('class', 'clickable');
        //Attention ring op de linkermouw
        attentionRing.setAttribute("visible", "true");
        linksOpvouwen(shirt);
    };

    let ringPositions = [
        { x: -1, z: 0.6 },
        { x: -1, z: -0.6 },
        { x: 0.05, z: 0 },
        { x: 2.2, z: 0 }
    ]     
    let ringPositionsBroek = [
        { x: -1, z: 0.2 },
        { x: 1.1, z: 0.1 },
        { x: -0.2, z: 0.1 },
        { x: 2.2, z: 0 }
    ]
    
    
    const strijken = kledingsstuk => {
        switch (kledingsstuk) {
            case kledingsstukkenShirt:
                responsiveVoice.speak(
                    'De grootste truc bij het strijken van een t-shirt, is om de strijkbout alleen maar neer te zetten op de stof en, er zo min mogelijk mee te bewegen. We beginnen met het strijken van de mouwen. Leg een mouw op de strijkplank en trek de mouw glad.',
                    taal,
                    rate
                ); 
                setPositionRing(ringPositions[0].x, ringPositions[0].z);
                setTimeout(function () {
                    attentionRing.setAttribute('visible', 'true');
                    ringHitbox.setAttribute('class', 'clickable');
                }, 12000); // TIJD MOET NADAT TEKST HIERBOVEN KLAAR IS
                
                setTimeout(function() {
                    stoomKnop.setAttribute('visible');
                    stoomKnopAchtergrond.setAttribute('class', 'clickable');
                },12000); // zelfde tijd als hierboven
                
                stoomKnopAchtergrond.onclick = () => {
                    clickStoom = true;
                    clearTimeout(timerId);
                    stoomKnopAchtergrond.setAttribute('color', 'green');
                    setTimeout(function () {
                        stoomKnopAchtergrond.setAttribute('color', 'black');
                    }, 3000);
                    particleFunction();
                }
            let clickCounter = 0;
            let clickStoom = false;
            ringHitbox.addEventListener("click", event => {
                if((clickStoom == false) && (clickCounter >= 1)) {
                    responsiveVoice.speak(
                        'Om goed en mooi te strijken, is het belangrijk dat het t-shirt een beetje vochtig is. Druk op de stoom knop.',
                        taal,
                        rate
                    ); 
                } else if (clickCounter <= 2){
                    ringHitbox.setAttribute('class', '');
                    animatieStrijken(ringPositions[clickCounter].x, ringPositions[clickCounter].z);
                    clickCounter++;
                    setTimeout(function() {
                        clearTimeout(timerId);
                        timerId = setInterval(countdown, 1000);
                        setPositionRing(ringPositions[clickCounter].x, ringPositions[clickCounter].z);    
                        ringHitbox.setAttribute('class', 'clickable');                        
                    },3010);
                } else if(clickCounter == 3){
                    clearTimeout(timerId);
                    setPositionRing(-1, -0.6);
                    stoomKnop.setAttribute('visible', 'false');
                    stoomKnopAchtergrond.setAttribute('class', '');
                    ringHitbox.remove();
                    attentionRing.setAttribute('class', '');
                    animateProperty(0, 180, 0, ijzer, 'rotation');
                    setTimeout(function () {
                        animateProperty(2.2, 2.7, 0, ijzer, 'position');
                    }, 2010);
                    mouwenOpvouwen(shirtKort);
                }
            });
            break;
        case kledingsstukkenShirtLang:
            responsiveVoice.speak(
                'Bij het strijken van een shirt met opdruk is het belangrijk om te zorgen dat het shirt binnenstebuiten is zodat je niet de opdruk beschadigd. Kijk naar de opdruk om het shirt binnenstebuiten te doen.',
                taal,
                rate
            ); 

            supremeLogo.setAttribute('class', 'clickable');
            supremeLogo.onclick = () => {
                supremeLogo.setAttribute('src', '#supremewazig');
                supremeLogo.setAttribute('class', '');
            }

            setPositionRing(ringPositions[0].x, ringPositions[0].z);
            setTimeout(function () {
                supremeLogo.setAttribute('src', '#supremewazig');
                attentionRing.setAttribute('visible', 'true');
                ringHitbox.setAttribute('class', 'clickable');
            }, 12000); // TIJD MOET NADAT TEKST HIERBOVEN KLAAR IS
            
            setTimeout(function() {
                stoomKnop.setAttribute('visible');
                stoomKnopAchtergrond.setAttribute('class', 'clickable');
            }, 12000); // zelfde tijd als hierboven

            let clickCounter2 = 0;
            let clickStoom2 = false;

            stoomKnopAchtergrond.onclick = () => {
                clickStoom2 = true;
                clearTimeout(timerId);
                stoomKnopAchtergrond.setAttribute('color', 'green');
                setTimeout(function () {
                    stoomKnopAchtergrond.setAttribute('color', 'black');
                }, 3000);
                particleFunction();
            }
            ringHitbox.addEventListener("click", event => {
                if((clickStoom2 == false) && (clickCounter2 >= 1)) {
                    responsiveVoice.speak(
                        'Om goed en mooi te strijken, is het belangrijk dat het shirt een beetje vochtig is. Druk op de stoom knop.',
                        taal,
                        rate
                    ); 
                } else if (clickCounter2 <= 2){
                    ringHitbox.setAttribute('class', '');
                    animatieStrijken(ringPositions[clickCounter2].x, ringPositions[clickCounter2].z);
                    clickCounter2++;
                    setTimeout(function() {
                        clearTimeout(timerId);
                        timerId = setInterval(countdown, 1000);
                        setPositionRing(ringPositions[clickCounter2].x, ringPositions[clickCounter2].z);    
                        ringHitbox.setAttribute('class', 'clickable');                        
                    },3010);
                } else if(clickCounter2 == 3){
                    clearTimeout(timerId);
                    setPositionRing(-1, -0.6);
                    ringHitbox.remove();
                    stoomKnop.setAttribute('visible', 'false');
                    stoomKnopAchtergrond.setAttribute('class', '');
                    attentionRing.setAttribute('class', '');
                    animateProperty(0, 180, 0, ijzer, 'rotation');
                    setTimeout(function () {
                        animateProperty(2.2, 2.7, 0, ijzer, 'position');
                    }, 2010);
                    mouwenOpvouwen(shirtLang);
                }
            });
            break;
        case kledingsstukkenBroek:
            responsiveVoice.speak(
                'Zorg ervoor dat de zakken leeg zijn van de broek. Leg de broek in de lengte op de strijkplank om de pijpen te strijken. Begin met het strijken van de zakken om nieuwe kreukels te voorkomen.',
                taal,
                rate
            ); 
            setPositionRing(ringPositionsBroek[0].x, ringPositionsBroek[0].z);
            setTimeout(function () {
                attentionRing.setAttribute('visible', 'true');
                ringHitbox.setAttribute('class', 'clickable');
            }, 12000); // TIJD MOET NADAT TEKST HIERBOVEN KLAAR IS
            
            setTimeout(function() {
                stoomKnop.setAttribute('visible');
                stoomKnopAchtergrond.setAttribute('class', 'clickable');
            }, 12000); // zelfde tijd als hierboven

            let clickCounter3 = 0;
            let clickStoom3 = false;

            stoomKnopAchtergrond.onclick = () => {
                clickStoom3 = true;
                clearTimeout(timerId);
                stoomKnopAchtergrond.setAttribute('color', 'green');
                setTimeout(function () {
                    stoomKnopAchtergrond.setAttribute('color', 'black');
                }, 3000);
                particleFunction();
            }
            ringHitbox.addEventListener("click", event => {
                if((clickStoom3 == false) && (clickCounter3 >= 1)) {
                    responsiveVoice.speak(
                        'Om het strijken nog makkelijker te maken kun je de broek een klein beetje vochtig maken. Druk op de stoom knop.',
                        taal,
                        rate
                    ); 
                } else if (clickCounter3 <= 2){
                    ringHitbox.setAttribute('class', '');
                    animatieStrijken(ringPositionsBroek[clickCounter3].x, ringPositionsBroek[clickCounter3].z);
                    clickCounter3++;
                    setTimeout(function() {
                        clearTimeout(timerId);
                        timerId = setInterval(countdown, 1000);
                        setPositionRing(ringPositionsBroek[clickCounter3].x, ringPositionsBroek[clickCounter3].z);    
                        ringHitbox.setAttribute('class', 'clickable');                        
                    },3010);
                } else if(clickCounter3 == 3){
                    clearTimeout(timerId);
                    setPositionRing(-1, 0.3);
                    stoomKnop.setAttribute('visible', 'false');
                    stoomKnopAchtergrond.setAttribute('class', '');
                    ringHitbox.remove();
                    broekspijpenRechts2.setAttribute('class', 'clickable');
                    attentionRing.setAttribute('class', '');
                    animateProperty(0, 180, 0, ijzer, 'rotation');
                    setTimeout(function () {
                        animateProperty(2.2, 2.7, 0, ijzer, 'position');
                    }, 2010);
                    broekOpvouwenBreedte();
                }
            });
            break;
        }
    }

    function once(fn, context) { 
        var result;
        return function() { 
            if(fn) {
                result = fn.apply(context || this, arguments);
                fn = null;
            }
            return result;
        };
    }

    const animatieStrijken = (x, z) => {
        animateProperty(x, ' 2.76 ', z, ijzer, 'position');
        setTimeout( once(function() {
            animateProperty(0, 270, 0, ijzer, 'rotation');
        }, 2050));
    }

    const setPositionRing = (x, z) => {
        attentionRing.setAttribute('position', `${x} 2.76 ${z}`);
        ringHitbox.setAttribute('position', `${x} 2.76 ${z}`);
    }

    const alarm = document.querySelector("#alarm");
    var timeLeft = 10;
    let timerId;
    function countdown() {
        if (timeLeft == 0) {
            clearTimeout(timerId);
            timeLeft = 10;
            alarm.play();
            let pos = {
                x: ijzer.getAttribute('position').x,
                z: ijzer.getAttribute('position').z
            }
            animateProperty(pos.x, '3.6', pos.z, ijzer, 'position');
            setTimeout(function () {
                alarm.pause();
                responsiveVoice.speak(
                    'Dat was te lang!',
                    taal,
                    rate
                ); 
            },3000);
            setTimeout(function () {
                animateProperty(pos.x, '2.76', pos.z, ijzer, 'position');
            },5000);
        } else {
            timeLeft--;
        }
    }


    // Stappen voor het opvouwen shirt korte mouwen
    const linksOpvouwen = (shirt) => {
        shirt.shirtLinkerMouw.addEventListener("click", event => {
            shirt.shirtLinkerMouw.setAttribute(
                "animation",
                " property: rotation; to: 180 0 0; loop: false; easing: linear; dur: 3000"
            );
            if (shirt.shirtMouwLOnder != null) {
                shirt.shirtMouwLOnder.setAttribute(
                    "animation",
                    "property: rotation; to: 180 0 0; loop: false; easing: linear; dur: 3000"
                )
            };
            console.log("linker mouw is bereikbaar");
            shirt.shirtRechterMouw.classList.replace("not-clickable", "clickable");
            //attentionring op de rechtermouw
            attentionRing.setAttribute("position", "-1 2.76 0.6");
            ringHitbox.setAttribute("position", "-1 2.76 0.6");
            rechtsOpvouwen(shirt);
        });
    };

    const rechtsOpvouwen = (shirt) => {
        shirt.shirtRechterMouw.addEventListener("click", event => {
            shirt.shirtRechterMouw.setAttribute(
                "animation",
                "property: rotation; to: -180 0 0; loop: false; easing: linear; dur: 3000"
            );
            if (shirt.shirtMouwROnder != null) {
                shirt.shirtMouwROnder.setAttribute(
                    "animation",
                    "property: rotation; to: -180 0 0; loop: false; easing: linear; dur: 3000"
                )
            }
            ;
            console.log("rechter mouw is bereikbaar");
            shirt.shirtOnderkant.classList.replace("not-clickable", "clickable");
            //attention op de onderkant van het shirt
            attentionRing.setAttribute("position", "0.05 2.76 0");
            onderOpvouwen(shirt);
        });
    };

    const onderOpvouwen = (shirt) => {
        shirt.shirtOnderkant.addEventListener("click", event => {
            shirt.shirtOnderkant.setAttribute(
                "animation",
                "property: rotation; to: 0 0 180; loop: false; easing: linear; dur: 3000"
            );
            let typeKleding;
            if(shirt.shirtMouwROnder != null){
                typeKleding = langShirt;
                shirt.shirtMouwROnder.remove();
                shirt.shirtMouwLOnder.remove();
            } else {
                typeKleding = kortShirt;
            };
            console.log("onderkant mouw is bereikbaar");
            attentionRing.setAttribute("visible", "false");
            attentionRing.remove();
            // stekkerMuur.setAttribute("class", "clickable");
            // stekkerPijl.setAttribute("visible", "true");
            kledingKast(typeKleding);
        });
    };

    // Broek opvouwen
    const broekOpvouwenBreedte = () => {
        broekspijpenRechts.addEventListener("click", event => {
            setTimeout(function (){
                broekspijpOnderLinks.setAttribute('class', 'clickable');
                attentionRing.setAttribute('position', '-0.2 2.8 0.1');
            },3000 )
            broekspijpBoven.setAttribute("animation", "property: rotation; to: 180 0 0; loop: false; easing: linear; dur:5000; ");
            broekspijpOnderLinks.setAttribute("animation", "property: rotation; to: 180 0 0; loop: false; easing: linear; dur:5000; ");
            broekLengteVouwenLinks();
        });
    };

    const broekLengteVouwenLinks = () => {
        broekspijpOnderLinks.addEventListener("click", event => {
            broekspijpOnderLinks.setAttribute('class', '');
            broekspijpOnderLinks.setAttribute("pivot", "0.4 2 0.125");
            broekspijpOnderLinks.setAttribute("animation", "property: rotation; to: 180 0 -180; loop: false; easing: linear; dur:10000; ");
            broekspijpOnderRechts.remove();
            attentionRing.remove();
            kledingKast(broek);
        })
    };

    const kledingKast = (kledingstuk) => {
        shirtStrijkLabel.remove();
        setTimeout(function () {
            animateProperty(-5.7, -0.75, 2.5, kledingstuk, 'position');
        }, 10500);
        setTimeout(function () {
            animateProperty(0, 90, 0, kastDeurL, 'rotation');
            end();
        }, 15000);
    };

    const end = () => {
        changeRoom();
        setTimeout(function () {
            teleportPlayer(0, 3);
        }, 10500);
        setTimeout(function() {
            location.reload();
        },20000);
    }

};
